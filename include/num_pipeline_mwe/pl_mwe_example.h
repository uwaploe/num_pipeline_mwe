#pragma once

#include "pl_routine.h"

namespace Numurus
{

class PLMWExample : public PLRoutine
{
public:

  // Configuration data
  struct PLMWExampleConfig : public PLRoutine::Config {
    int configValue;
  };

  PLMWExample();
  virtual ~PLMWExample();

  virtual bool configure(const PLRoutine::Config &config);

  virtual bool process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out);
};

}
