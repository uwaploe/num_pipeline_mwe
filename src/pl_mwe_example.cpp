#include "num_pipeline_mwe/pl_mwe_example.h"

namespace Numurus
{

  PLMWExample::PLMWExample()
    : PLRoutine()
  {;}

  PLMWExample::~PLMWExample()
  {;}

  bool PLMWExample::configure(const PLRoutine::Config &config)
  {
    return PLRoutine::configure(config);
  }

  bool PLMWExample::process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out)
  {
    if( !readyToProcess() ) {
      log(PL_WARN, "PLMWExample::process called when not ready to process");
      return false;
    }

    /*
     * This routine does nothing but return the data and quality unchanged.
     */
    *data_out = data_in->clone();
    *quality_out = quality_in;
    return true;
  }

}
