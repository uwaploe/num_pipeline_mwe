// Bring in my package's API, which is what I'm testing
#include "num_pipeline_mwe/pl_mwe_example.h"

// Bring in gtest
#include <gtest/gtest.h>

using namespace Numurus;

// Declare a test
TEST(TestMWExample, testCase1)
{
  PLMWExample *example( new PLMWExample );

  ASSERT_NE( example, nullptr );

  delete example;
}
