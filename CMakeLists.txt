cmake_minimum_required(VERSION 2.8.3)
project(num_pipeline_mwe)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)
add_compile_options(-Wall)

## Require the ROS OpenCV bundle
set(OpenCV_DIR /opt/ros/kinetic/share/OpenCV-3.3.1-dev)
find_package(catkin REQUIRED COMPONENTS roscpp num_pl_core)
find_package(OpenCV REQUIRED)

#set(CMAKE_VERBOSE_MAKEFILE ON)

## Output goes to /lib subfolder
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ./lib)

catkin_package(
    INCLUDE_DIRS include
    LIBRARIES ${PROJECT_NAME}
    CATKIN_DEPENDS num_pl_core
)

###########
## Build ##
###########

## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
    src/pl_mwe_example.cpp
)

## Add unit tests
catkin_add_gtest( utest
                test/main.cpp
                test/test_dummy.cpp )
target_link_libraries( utest ${PROJECT_NAME} ${catkin_LIBRARIES} ${OpenCV_LIBS} )

## Mark executables and/or libraries for installation
install(TARGETS ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Non-generated headers are located directly in include and get copied to the root of include in the install directory
install(DIRECTORY include/
  DESTINATION ${CATKIN_GLOBAL_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h"
)
