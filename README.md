# Minimum working example of Numurus Pipeline processing

Builds a minimum working example of a Numurus Pipeline library processing
node.  Depends on [num_pl_core](https://bitbucket.org/numurus/num_pl_core.git).
